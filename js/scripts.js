(function($, Drupal) {

	Drupal.ThemesName = Drupal.ThemesName || {};
	Drupal.ThemesName.isMobileSize = false;

	Drupal.behaviors.actionThemesName = {
		attach: function(context) {
			Drupal.ThemesName.documentReady();
			Drupal.ThemesName.mobileMenu();
			Drupal.ThemesName.backToTop();
		}
	};

	Drupal.ThemesName.documentReady = function() {
		$(document).ready(function() {
			if ($(window).width() <= 991) {
				Drupal.ThemesName.isMobileSize = true;
			}
		});
	}

	Drupal.ThemesName.mobileMenu = function() {
		$('.navbar-toggle').mobileMenu({
			targetWrapper: '.header .menu',
		});
	}

	Drupal.ThemesName.backToTop = function() {
		$(window).load(function () {
			if($(this).scrollTop()){
				$('.btn-btt').fadeIn();
			}
			else{
				$('.btn-btt').fadeOut();
			}
		});
		$(window).scroll(function () {
			if($(this).scrollTop()){
				$('.btn-btt').fadeIn();
			}
			else{
				$('.btn-btt').fadeOut();
			}
		});

		$('.btn-btt').click(function () {
			$('html, body').animate({scrollTop : 0}, 700);
			return false;
		})
	};

	Drupal.ThemesName.windowResize = function() {
		$(window).resize(function() {
			if ($(window).width() <= 991) {
				Drupal.ThemesName.isMobileSize = true;
			} else {
				Drupal.ThemesName.isMobileSize = false;
			}   
		});
	}

	$(document).ready(function() {});

})(jQuery, Drupal);
